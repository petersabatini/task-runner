import pgPromise from "pg-promise";
import cron from 'node-cron';
import moment from "moment";
import dotenv from 'dotenv';

dotenv.config();

const connection = pgPromise()(process.env.CONNECTION_STRING);

let jobs = [];

/* 
This function mocks a task running. This would be a dispatcher to different services
depending on the task type if is communication it will call something like 

Ex: communicationService.sendMessage() ...

This function also logs the running of a tast to audit and monitor.
*/
const runTask = async (task) => {
  console.log('Running task', task.task_id);
  await connection.none(`
    INSERT INTO task_crons_history (task_id, name, type, schedule, action, params)
    SELECT task_id, name, type, schedule, action, params 
    FROM task_crons WHERE task_id = $(taskId)
  `, {
    taskId: task.task_id,
  });
};

const deleteTask = async (task) => {
  await connection.none(
    'DELETE FROM task_crons WHERE task_id = $(taskId)', 
    { taskId: task.task_id }
  );
};

const scheduleTask = async (task) => {
  jobs.push(task.task_id);
  console.log('Setting Up job', task.task_id);
  cron.schedule(task.schedule, () => {
    runTask(task);
  }); 
}

// Upon service start we initiate interval to check for new task to be scheduled every 10 seconds
setInterval(async () => {
  const taskCrons = await connection.any('SELECT * FROM task_crons');
  console.log('Check for new tasks', new Date());

  taskCrons.map((task) => {
    if (!jobs.includes(task.task_id)) {

      if (task.type === 'recurring') {
        scheduleTask(task);
      }

      if (task.type === 'one-time' && moment().isAfter(moment(task.runtime))) {
        runTask(task);
        deleteTask(task);
      }
    }
  });
}, 10000);