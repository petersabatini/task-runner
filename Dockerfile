FROM node:20-alpine

WORKDIR /app

COPY package*.json ./

RUN npm i npm@latest -g
RUN npm install --only=production

COPY . .

CMD [ "npm", "start" ]
