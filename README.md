Prerequisites
-----

I assume you have installed Docker and it is running.

See the [Docker website](http://www.docker.io/gettingstarted/#h_installation) for installation instructions.

You will also need to have your database already setup before attempting to start this

Run Project with Docker
-----

1. Clone this repo

        git clone https://gitlab.com/petersabatini/task-runner.git

2. Build the image

        cd ..
        docker build -t task-runner .

    This will take a few minutes.

3. Run the image - You will need to configure CONNECTION_STRING to match your database settings. If you have your database setup locally you will need to use `host.docker.internal` if you want to access your machine actual host

        docker run -d -e CONNECTION_STRING='postgres://petersabatini:@host.docker.internal:5433/task_scheduler' task-runner

4. Validate that the image is running and read logs

        docker ps -a
        docker logs ${CONTAINER_ID}

Run Project with Node / NVM
-----

1. Clone this repo

        git clone https://gitlab.com/petersabatini/task-runner.git

2. Create a root file called `.env` and put a configuration like this to match your database connection

        CONNECTION_STRING="postgres://petersabatini:@localhost:5433/task_scheduler"

3. This project uses Node 20. You can install `nvm` to manage multiple versions of node

        cd ..
        nvm use 20
        npm install
        npm run start